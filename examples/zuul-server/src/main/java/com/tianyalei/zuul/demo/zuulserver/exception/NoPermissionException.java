package com.tianyalei.zuul.demo.zuulserver.exception;

/**
 * 统一异常
 * @author ratel 2022-04-10
 */

public class NoPermissionException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private String msg;
    private int code = 401;

    public NoPermissionException() {
        this.msg="无权限访问，请联系管理员授权！";
    }
    public NoPermissionException(int code , String msg) {
        super(msg);
        this.code=code;
        this.msg=msg;
    }
    public NoPermissionException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public NoPermissionException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }

    public NoPermissionException(String msg, int code) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public NoPermissionException(String msg, int code, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


}
