package com.tianyalei.zuul.demo.zuulserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 *基于zuul的demo
 * demo工程-zuul网关，用于统一的鉴权服务
 * @author ratel 2022-04-10
 */
@SpringBootApplication
@EnableZuulProxy
public class ZuulServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZuulServerApplication.class, args);
        System.out.println("启动成功！");
    }
}
