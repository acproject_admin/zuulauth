
package com.tianyalei.zuul.demo.authserver.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.tianyalei.zuul.demo.authserver.entity.UserEntity;
import com.tianyalei.zuul.demo.authserver.form.LoginForm;

/**
 * 用户
 *
 * @author ratel
 */
public interface UserService extends IService<UserEntity> {

	UserEntity queryByMobile(String mobile);

	/**
	 * 用户登录
	 * @param form    登录表单
	 * @return        返回用户ID
	 */
	long login(LoginForm form);
}
