
package com.tianyalei.zuul.demo.authserver.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tianyalei.zuul.demo.authserver.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户
 *
 * @author ratelfu
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {

}
