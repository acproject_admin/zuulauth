package com.tianyalei.zuul.demo.authserver.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Role implements Serializable {

    private long roleId;

    private String roleName;

    private String roleCode;
}
