package com.tianyalei.zuul.demo.authserver.entity;


import java.io.Serializable;

public class Permission implements Serializable {

    private long permissionId;

    private String uri;

    private String url;

    public long getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(long permissionId) {
        this.permissionId = permissionId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}

