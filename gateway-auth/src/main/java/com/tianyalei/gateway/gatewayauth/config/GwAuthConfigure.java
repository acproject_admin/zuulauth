package com.tianyalei.gateway.gatewayauth.config;

import com.tianyalei.core.zuulauth.config.AuthConfigure;
import com.tianyalei.core.zuulauth.config.ClientAuthCacheConfigure;
import com.tianyalei.core.zuulauth.zuul.AuthInfoHolder;
import com.tianyalei.gateway.gatewayauth.gateway.AuthChecker;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.gateway.config.GatewayAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;

/**
 * @author liyunfeng 2020-1-14
 */
@Configuration
@ConditionalOnClass(GatewayAutoConfiguration.class) //这一句是当前工程是Gateway工程时，才启用该configuration
public class GwAuthConfigure extends AuthConfigure {


    @Resource
    private ApplicationContext applicationContext;

    @Bean
    @ConditionalOnMissingBean
    AuthChecker authChecker() {
        logger.info("============ GATEWAY authChecker load ============="+applicationContext);
        return new AuthChecker(applicationContext);
    }

}
